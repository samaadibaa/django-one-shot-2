from django.shortcuts import render, get_list_or_404, redirect
from todos.models import TodoList, TodoItem

# Create your views here.


def show_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "lists": todo_lists,
    }
    return render(request, "todos/main-page.html", context)
