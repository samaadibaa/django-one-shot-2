from django.urls import path
from todos.views import show_list


urlpatterns = [
    path("", show_list, name="todo_list_list"),
]
